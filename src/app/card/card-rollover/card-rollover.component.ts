import { SpinnerServiceService } from './../../spinner/spinner-service.service';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-card-rollover',
  templateUrl: './card-rollover.component.html',
  styleUrls: ['./card-rollover.component.css']
})
export class CardRolloverComponent implements OnInit, AfterViewInit {

  @ViewChild('video') public playerElement: ElementRef;

  constructor(public ss: SpinnerServiceService) { }

  ngAfterViewInit(): void {
    this.playerElement.nativeElement.play();
    setTimeout(() => {
      this.ss.emiter.next(true)
    }, 1000)
  }

  ngOnInit(): void {

  }



}
