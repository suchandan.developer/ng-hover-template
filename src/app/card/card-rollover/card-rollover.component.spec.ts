import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardRolloverComponent } from './card-rollover.component';

describe('CardRolloverComponent', () => {
  let component: CardRolloverComponent;
  let fixture: ComponentFixture<CardRolloverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardRolloverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardRolloverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
