import { SpinnerServiceService } from './../spinner/spinner-service.service';
import { Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  public showRollover = false;
  public rolloverLoaded = false;
  public rollOverStyle: any = {};

  @Input('container') public container: HTMLElement;

  @ViewChild('rollover') public rollover: ElementRef;
  @ViewChild('card') public card: ElementRef;

  constructor(public ss: SpinnerServiceService) { }

  @HostListener('mousemove', ['$event'])
  onmove(event: MouseEvent) {
    this.ss.spinner.next({
      show: !this.rolloverLoaded,
      position: {
        x: event.clientX,
        y: event.clientY
      }
    })
  }

  @HostListener('mouseover', ['$event'])
  onhover(event: MouseEvent) {
    // console.log("EVENT", event)
    this.showRollover = true;
    this.ss.spinner.next({
      show: !this.rolloverLoaded,
      position: {
        x: event.clientX,
        y: event.clientY
      }
    })
  }

  @HostListener('mouseleave', ['$event'])
  onleave(event: MouseEvent) {
    this.rolloverLoaded = false;
    this.showRollover = false;
    this.rollOverStyle = {}
    this.ss.spinner.next({
      show: false,
      position: {
        x: event.clientX,
        y: event.clientY
      }
    })
  }

  ngOnInit(): void {
    this.ss.emiter.asObservable().subscribe((data) => {

      try {
        let el: any = this.rollover as any;
        let player: HTMLElement = el.playerElement.nativeElement;
        let card: HTMLElement = this.card.nativeElement;

        let containerPos = this.container.getBoundingClientRect();
        let cardPos = card.getBoundingClientRect();
        // console.dir(player)
        console.log("CONTAINER")
        console.dir(this.container.getBoundingClientRect())

        // console.dir(player.getBoundingClientRect())
        console.log("CARD")
        console.dir(card.getBoundingClientRect())

        let diff = {
          top: cardPos.top - containerPos.top,
          right: containerPos.right - cardPos.right,
          bottom: containerPos.bottom - cardPos.bottom,
          left: cardPos.left - containerPos.left
        }
        console.log(diff);

        let pos: any = {};

        let padding = 15;

        if (diff.top < padding && diff.right < padding) {
          this.rollOverStyle.left = (containerPos.width - cardPos.width) - player.clientWidth + 'px';
          this.rollOverStyle.top = diff.top + 'px';
        }

        this.rolloverLoaded = true;
        this.ss.spinner.next({
          show: false,
          position: {
            x: 0,
            y: 0
          }
        })
      } catch (e) {

      }

    })
  }

}
