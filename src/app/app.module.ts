import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { CardRolloverComponent } from './card/card-rollover/card-rollover.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    CardRolloverComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
