import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerServiceService {

  public spinner: Subject<any> = new Subject();
  public emiter: Subject<any> = new Subject();

  constructor() { }
  
}
