import { SpinnerServiceService } from './spinner/spinner-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'test-mouseover';
  spinner = false;
  spinnerStyle: any = { "left": 0, "top": 0 };
  constructor(public ss: SpinnerServiceService) { }

  ngOnInit(): void {
    this.ss.spinner.asObservable().subscribe(({ show, position }) => {
      this.spinner = show;
      this.spinnerStyle = {
        "left": position.x + 12 + 'px',
        "top": position.y + 20  + 'px'
      }
      // console.log("this.spinnerStyle", this.spinnerStyle)
    })
  }


}
